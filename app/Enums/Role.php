<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static KARYAWAN()
 * @method static static MANAGER()
 * @method static static SENIORMANAGER()
 */
final class Role extends Enum
{
    const KARYAWAN =   'KARYAWAN';
    const MANAGER =   'MANAGER';
    const SENIORMANAGER = 'SENIOR MANAGER';
}
