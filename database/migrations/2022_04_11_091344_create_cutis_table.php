<?php

use App\Enums\Status;
use App\Enums\TipeCuti;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutis', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->text('alasan');
            $table->date('tanggal_mulai_cuti');
            $table->date('tanggal_selesai_cuti');
            $table->enum('jenis_cuti', TipeCuti::asArray());
            $table->string('bukti_pengajuan')->nullable();
            $table->enum('is_acc_manager', Status::asArray());
            $table->string('is_acc_manager_by')->nullable();
            $table->enum('is_acc_senior_manager', Status::asArray());
            $table->string('is_acc_senior_manager_by')->nullable();
            $table->foreign('user_id')->references('id')->on('users')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutis');
    }
};
