@extends('layout.main')

@section('custom-css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('container')

<div class="row">
    <div class="col-lg-8 mx-auto">
        <div class="card shadow mb-4 border-left-primary">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Pengajuan Cuti</h6>
            </div>
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{ url('/cuti') }}">
                    @csrf
                    <div class="mb-3">
                        <label for="tanggal-cuti" class="form-label">Tanggal Cuti</label>
                        <div type="text" class="form-control" id="tanggal-cuti">
                            <div class="row">
                                <div class="col-lg-11">
                                    <i class="fa-solid fa-calendar-days mr-3"></i>
                                    <span id="now">14-03-2022</span>
                                </div>
                                <div class="col-lg-1">
                                    <i class="fa fa-caret-down"></i>
                                </div>
                            </div>
                        </div>
                        @error('tanggal_mulai_cuti')
                        <div class="invalid-feedback">
                            {{ $message }}
                            @error('tanggal_selesai_cuti')
                            {{ $message }}
                            @enderror
                        </div>
                        @enderror
                    </div>
                    <input type="text" name="tanggal_mulai_cuti" id="tanggal-mulai" hidden>
                    <input type="text" name="tanggal_selesai_cuti" id="tanggal-selesai" hidden>
                    <div class="mb-3">
                        <label for="alasan" class="form-label">Alasan</label>
                        <input type="text" class="form-control @error('alasan') is-invalid @enderror" id="alasan" name="alasan">
                        @error('alasan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="bukti" class="form-label">Bukti Pengajaun</label>
                        <img class="img-preview img-fluid mb-3 col-sm-5">
                        <input class="form-control @error('bukti_pengajuan') is-invalid @enderror" type="file" id="bukti" name="bukti_pengajuan" onchange="previewImage()">
                        @error('bukti_pengajuan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary d-flex justify-content-end">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom-js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
    $(function() {

        $('span#now').text(moment().format('DD-MM-YYYY') + " - " +
            moment().format('DD-MM-YYYY'))
        $('input#tanggal-mulai').val(moment().format('YYYY-MM-DD'))
        $('input#tanggal-selesai').val(moment().format('YYYY-MM-DD'))

        $('#tanggal-cuti').daterangepicker({
            opens: 'left',
            minDate: moment().format('DD-MM-YYYY')
        }, function(start, end, label) {
            $('input#tanggal-mulai').val(start.format('YYYY-MM-DD'))
            $('input#tanggal-selesai').val(end.format('YYYY-MM-DD'))
            $('span#now').text(start.format('DD-MM-YYYY') + " - " +
                end.format('DD-MM-YYYY'))

        });
    });
</script>

<script>
    function previewImage() {
        const image = document.querySelector('#bukti');
        const imgPreview = document.querySelector('.img-preview');

        imgPreview.style.display = 'block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(image.files[0]);

        oFReader.onload = function(oFREvent) {
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>

@endsection