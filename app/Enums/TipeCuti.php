<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static MELAHIRKAN()
 * @method static static VACATION()
 * @method static static OptionThree()
 */
final class TipeCuti extends Enum
{
    const MELAHIRKAN =   "MELAHIRKAN";
    const VACATION =   "VACATION";
    const LAINLAIN = "LAIN-LAIN";
}
