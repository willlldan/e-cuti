<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register.index', [
            'title' => 'Register',
            'active' => 'register'
        ]);
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'nik' => 'required|max:20',
            'name' => 'required|min:3',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);


        User::create($validatedData);

        return redirect('/login')->with('success', 'Registration successfull! Please Login');
    }
}
