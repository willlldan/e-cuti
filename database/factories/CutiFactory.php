<?php

namespace Database\Factories;

use App\Enums\TipeCuti;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cuti>
 */
class CutiFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {

        return [
            'user_id' => $this->faker->numberBetween(1, 10),
            'alasan' => $this->faker->text(200),
            'tanggal_mulai_cuti' => $this->faker->date(),
            'tanggal_selesai_cuti' => $this->faker->date(),
            'jenis_cuti' => TipeCuti::getRandomValue(),
            'bukti_pengajuan' => null,
        ];
    }
}
