<?php

namespace App\Http\Controllers;

use App\Enums\Role;
use App\Enums\Status;
use App\Models\Cuti;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuti = DB::table('cutis')
            ->select(['cutis.*', 'users.nik', 'users.name'])
            ->join('users', 'users.id', '=', 'cutis.user_id')
            ->where('cutis.user_id', '=', auth()->user()->id)
            ->get();

        return view('cuti.index', [
            'title' => "Daftar Cuti",
            'cuti' => $cuti,
            'status' => Status::asArray()
        ]);
    }

    public function approval()
    {
        if (auth()->user()->role == Role::SENIORMANAGER()) {
            $cuti = DB::table('cutis')
                ->select(['cutis.*', 'users.nik', 'users.name'])
                ->join('users', 'users.id', '=', 'cutis.user_id')
                ->where('cutis.is_acc_manager', '=', Status::APPROVED())
                ->get();
        } else {
            $cuti = DB::table('cutis')
                ->select(['cutis.*', 'users.nik', 'users.name'])
                ->join('users', 'users.id', '=', 'cutis.user_id')
                ->get();
        }

        return view('cuti.index', [
            'title' => "Daftar Cuti",
            'cuti' => $cuti,
            'status' => Status::asArray()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cuti.create', [
            "title" => "Pengajuan Cuti"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'tanggal_mulai_cuti' => 'required|max:255',
            'tanggal_selesai_cuti' => 'required|max:255',
            'alasan' => 'required',
            'bukti_pengajuan' => 'image|file|max:1024'
        ]);

        if ($request->file('bukti_pengajuan')) {
            $validateData['bukti_pengajuan'] = $request->file('bukti_pengajuan')->store('bukti_pengajuan');
        }

        $validateData['user_id'] = auth()->user()->id;

        Cuti::create($validateData);

        return redirect('/cuti')->with('success', 'Berhasil mengajukan cuti. Tunggu persetujuan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function show(Cuti $cuti)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuti $cuti)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuti $cuti)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuti $cuti)
    {
        if ($cuti->bukti_pengajuan) {
            Storage::delete($cuti->bukti_pengajuan);
        }

        Cuti::destroy($cuti->id);
        return redirect()->back()->with('success', 'Berhasil menghapus cuti.');
    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, Cuti $cuti)
    {
        if (auth()->user()->role == "MANAGER") {
            $cuti['is_acc_manager'] = $request->status;
            $cuti['is_acc_manager_by'] = auth()->user()->email;
        } else {
            $cuti['is_acc_senior_manager'] = $request->status;
            $cuti['is_acc_senior_manager_by'] = auth()->user()->email;
        };



        $cuti->save();

        return redirect()->back()->with('success', 'Berhasil update status cuti.');
    }
}
