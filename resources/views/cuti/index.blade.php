@extends('layout.main')

@section('custom-css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
@endsection



@section('container')

<div class="row">
    <div class="card shadow mb-4 border-left-primary">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Pengajuan Cuti</h6>
        </div>
        <div class="card-body">
            @if (session()->has('success'))
            <div class="alert alert-success col-lg-8" role="alert">
                {{ session('success') }}
            </div>
            @endif
            <div class="col-lg-12 table-responsive" style="font-size: 0.75rem;">
                <table id="table_id" class="display table">
                    <thead>
                        <tr class="text-center">
                            <th>No</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Tanggal Mulai Cuti</th>
                            <th>Tanggal Selesai Cuti</th>
                            <th>Jenis Cuti</th>
                            <th>Acc Manager</th>
                            <th>Acc Senior Manager</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="table_body">
                        @foreach($cuti as $cuti)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $cuti->nik }}</td>
                            <td>{{ $cuti->name }}</td>
                            <td>{{ $cuti->tanggal_mulai_cuti }}</td>
                            <td>{{ $cuti->tanggal_selesai_cuti }}</td>
                            <td>{{ $cuti->jenis_cuti }}</td>
                            <td>
                                @if(auth()->user()->role == 'MANAGER')
                                <div class="dropdown">
                                    @if($cuti->is_acc_manager == $status['WAITING'])
                                    <button style="font-size: 0.75rem;" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        {{ $cuti->is_acc_manager }}
                                    </button>
                                    @elseif(($cuti->is_acc_manager == $status['APPROVED']))
                                    <button style="font-size: 0.75rem;" class="btn btn-success dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">{{ $cuti->is_acc_manager }}</button>
                                    @else
                                    <button style="font-size: 0.75rem;" class="btn btn-danger dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">{{ $cuti->is_acc_manager }}</button>
                                    @endif
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" id="status">
                                        <li><a class="dropdown-item" href="#" data-id="{{$cuti->id}}" data-status="{{$status['APPROVED']}}">{{$status['APPROVED']}}</a></li>
                                        <li><a class="dropdown-item" href="#" data-id="{{$cuti->id}}" data-status="{{$status['WAITING']}}">{{$status['WAITING']}}</a></li>
                                        <li><a class="dropdown-item" href="#" data-id="{{$cuti->id}}" data-status="{{$status['REJECTED']}}">{{$status['REJECTED']}}</a></li>
                                    </ul>
                                </div>
                                @else
                                @if($cuti->is_acc_manager == $status['WAITING'])
                                <div>
                                    <button style="font-size: 0.75rem;" class="btn btn-secondary" type="button">
                                        {{ $cuti->is_acc_manager }}
                                    </button>
                                    @elseif(($cuti->is_acc_manager == $status['APPROVED']))
                                    <button style="font-size: 0.75rem;" class="btn btn-success">{{ $cuti->is_acc_manager }}</button>
                                    @else
                                    <button style="font-size: 0.75rem;" class="btn btn-danger">{{ $cuti->is_acc_manager }}</button>
                                </div>
                                @endif
                                @endif



                            </td>
                            <td>
                                @if(auth()->user()->role == 'SENIOR MANAGER')
                                <div class="dropdown">
                                    @if($cuti->is_acc_senior_manager == $status['WAITING'])
                                    <button style="font-size: 0.75rem;" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        {{ $cuti->is_acc_senior_manager }}
                                    </button>
                                    @elseif(($cuti->is_acc_senior_manager == $status['APPROVED']))
                                    <button style="font-size: 0.75rem;" class="btn btn-success dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">{{ $cuti->is_acc_senior_manager }}</button>
                                    @else
                                    <button style="font-size: 0.75rem;" class="btn btn-danger dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">{{ $cuti->is_acc_senior_manager }}</button>
                                    @endif
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" id="status">
                                        <li><a class="dropdown-item" href="#" data-id="{{$cuti->id}}" data-status="{{$status['APPROVED']}}">{{$status['APPROVED']}}</a></li>
                                        <li><a class="dropdown-item" href="#" data-id="{{$cuti->id}}" data-status="{{$status['WAITING']}}">{{$status['WAITING']}}</a></li>
                                        <li><a class="dropdown-item" href="#" data-id="{{$cuti->id}}" data-status="{{$status['REJECTED']}}">{{$status['REJECTED']}}</a></li>
                                    </ul>
                                </div>
                                @else
                                @if($cuti->is_acc_senior_manager == $status['WAITING'])
                                <div>
                                    <button style="font-size: 0.75rem;" class="btn btn-secondary" type="button">
                                        {{ $cuti->is_acc_senior_manager }}
                                    </button>
                                    @elseif(($cuti->is_acc_senior_manager == $status['APPROVED']))
                                    <button style="font-size: 0.75rem;" class="btn btn-success">{{ $cuti->is_acc_senior_manager }}</button>
                                    @else
                                    <button style="font-size: 0.75rem;" class="btn btn-danger">{{ $cuti->is_acc_senior_manager }}</button>
                                </div>
                                @endif
                                @endif
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form action="" class="d-inline-block">
                                            <button style="font-size: 0.75rem;" class="btn btn-rounded btn-primary"><i class="fa-solid fa-pen-to-square"></i></button>
                                        </form>
                                    </div>
                                    <div class="col-lg-4">
                                        <form action="{{ url('/cuti/' . $cuti->id) }}" class="d-inline-block" method="post">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" style="font-size: 0.75rem;" class="btn btn-rounded btn-danger" onclick="return confirm('Are you sure?')"><i class="fa-solid fa-trash"></i></button>
                                        </form>
                                    </div>
                                    <div class="col-lg-4">
                                        <button type="button" style="font-size: 0.75rem;" class="btn btn-rounded btn-success" data-bs-toggle="modal" data-bs-target="#detailModal" data-bs-karyawan="{{$cuti->name}}" data-bs-alasan="{{$cuti->alasan}}" data-bs-buktiPengajuan="{{$cuti->bukti_pengajuan}}"><i class="fa-solid fa-eye"></i></button>
                                    </div>
                                </div>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Form Update status -->

<form action="" method="post" id="update-status">
    @csrf
    <input type="hidden" id="approve" name="status">
</form>


@endsection

@section('custom-js')

<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Detail Pengajuan Cuti </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table>
                    <tr>
                        <td>Alasan</td>
                        <td class="alasan-value"></td>
                    </tr>
                    <tr>
                        <td>Bukti Pengajuan</td>
                        <td class="bukti-pengajuan"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/plug-ins/1.11.5/dataRender/ellipsis.js"></script>

<script>
    $(document).ready(function() {
        $('#table_id').DataTable({
            columnDefs: [{
                width: '15%',
                targets: 2
            }],
        });
    });

    $('#detailModal').on('show.bs.modal', function(event) {
        var button = event.relatedTarget
        var karyawan = button.getAttribute('data-bs-karyawan')
        var alasan = button.getAttribute('data-bs-alasan')
        var buktiPengajuan = button.getAttribute('data-bs-buktiPengajuan')
        $('.modal-title').text('Detail Pengajuan Cuti' + karyawan)
        $('.alasan-value').text(alasan)
        $('.bukti-pengajuan').text(buktiPengajuan)
    })

    $('#status > li > a').on('click', function(e) {
        var id = $(this).data('id')
        var status = $(this).data('status')

        $('form#update-status').attr('action', `{{ url('/cuti/approval') }}/${id}`)
        $('input#approve').val(status)
        $('form#update-status').submit()
    })
</script>


@endsection