<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        \App\Models\Cuti::factory(10)->create();
        \App\Models\User::create([
            'nik' => '321001001001111',
            'name' => 'Wildan Fauzi',
            'role' => 'MANAGER',
            'email' => 'wil@gmail.com',
            'password' => Hash::make('123456')
        ]);
        \App\Models\User::create([
            'nik' => '333222111',
            'name' => 'Asep Jaelani',
            'role' => 'SENIOR MANAGER',
            'email' => 'asep@gmail.com',
            'password' => Hash::make('123456')
        ]);
        \App\Models\User::create([
            'nik' => '333222111',
            'name' => 'John Oscar',
            'role' => 'KARYAWAN',
            'email' => 'john@gmail.com',
            'password' => Hash::make('123456')
        ]);
    }
}
