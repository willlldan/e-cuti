<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static WAITING()
 * @method static static REJECTED()
 * @method static static APPROVED()
 */
final class Status extends Enum
{
    const WAITING =   "WAITING";
    const REJECTED =   "REJECTED";
    const APPROVED = "APPROVED";
}
