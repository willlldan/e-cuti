# E-cuti

## Getting started

-   Install Composer

```
composer install
```

-   Create env
    Copy paste .env.example and rename to .env. After that, create key

```
php artisan key:generate
```

-   Prepare Database & Seeder

```
php artisan migrate:fresh
```

```
php artisan db:seed Database Seeder
```

Demo User

-   Manager
    email : john@gmail.com
    password : 123456

-   Manager
    email : wil@gmail.com
    password : 123456

-   Senior Manager
    email : asep@gmail.com
    password : 123456
